import React from 'react';
import ReactDOM from 'react-dom';

class Form extends React.Component {

  constructor(props){
    super(props);

    this.state={
      newItem: "",
      list:[],
      newItem2: "",
      list2:[],
    };
  }

updateInput(key, value){
  this.setState({
    [key]: value
  });
}
   
newItem(e){
  e.preventDefault();

};

addItem(){
    const newItem={
      id: 1 + Math.random(),
      value: this.state.newItem.slice()
    };

    const list = [...this.state.list];

    list.push(newItem);

    this.setState({
      list,
      newItem:""
    });
}

addItem2(){

  const newItem2={
    id: 2 + Math.random(),
    value: this.state.newItem2.slice()
  };

  const list2 = [...this.state.list2];


  list2.push(newItem2);
  this.setState({
    list2,
    newItem2:""
  });

 

}





  deleteItem(id){
    const list = [...this.state.list];

    const updatedList = list.filter(item => item.id !== id);

    this.setState({
      list: updatedList,
    });
  };


  deleteItem2(id){
    const list2 = [...this.state.list2];

    const updatedList2 = list2.filter(item => item.id !== id);

    this.setState({
      list2: updatedList2,
    });
  };


   




  

    render() {
      return (    
        <div>
          <h2>Todo list</h2>
          <form onSubmit={(e) => {this.newItem(e)}}>
          <input type="text" placeholder="Input to do" className="todo-input" value={this.state.newItem} onChange={e => this.updateInput("newItem", e.target.value)} />

          <button className="todo-button" onClick={() => this.addItem()}>Levo</button>
          <button className="todo-button" onClick={() => this.addItem2()}>Desno</button>


          </form>
          <div className="card">

          <div className="card1">
          <p>To do</p>
          <hr></hr>
          <div className="card-components">
            {this.state.list.map(item =>{
              return(
                <li key={item.id}>
                  <button
                  onClick={() => this.deleteItem(item.id)} >
                    Delete
                  </button>
                  {item.value}
                  
                </li>
              )
            })}</div>

          </div>

          <div className="card2">
          <p>in progress</p>
          <hr></hr>
          <div className="card-components2">
         {this.state.list2.map(item => {
             return (
               <li key={item.id}>
              
                 {item.value}
                 <button
                  onClick={() => this.deleteItem2(item.id)} >
                    Delete
                  </button>
                 
               </li>
             )
           })
         }

         


          </div>
          </div>
 
          

          </div>
        </div>
        
      );
    }
  }


 

export default Form;