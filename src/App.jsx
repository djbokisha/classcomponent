import React from 'react';
import './App.css';


//components

import Form from "./components/Form";
import Todo from './components/Todo';



function App() {
 
  return (
    <div className="App">
      
      <header className="App-header">
      <Form />
      </header>

      <main>
      <Todo />
      </main>

    </div>
  );
}

export default App;